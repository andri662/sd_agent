﻿namespace SD_AgentService
{
    public struct Drive
    {
        public string Caption;
        public string DeviceID;
        public int Partitions;
        public long Size;
        public string SerialNumber;
    }

    public struct LogicalDrive
    {
        public string Name;
        public string VolumeLabel;
        public long Size;
        public long FreeSpace;
    }

    public struct LogicalVolume
    {
        public string Letter;
        public string Label;
        public long Size;
        public long FreeSpace;
    }

    public struct BaseSystem
    {
        public string AgentVersion;
        public string OSName;
        public string OSVersion;
        public string Baseboard;
        public string BaseboardSerial;
        public string SDState;
        public string TFState;
    }

    public struct NetworkData
    {
        public string IPv4;
        public string DNS1;
        public string DNS2;
        public string HardwareAddress;
        public string Hostname;
    }

    public struct DIMM
    {
        public string BankLabel;
        public long Capacity;
        public string DeviceLocator;
        public string PartNumber;
        public string SerialNumber;
        public string Manufacturer;
        public int Speed;
    }

    public struct Processor
    {
        public string ProcessorID;
        public int MaxClockSpeed;
        public int CurrentClockSpeed;
        public double Usage;
        public string Name;
        public double PhysicalCores;
        public double LogicalCores;
    }

    public struct VideoProcessor
    {
        public string Description;
        public string DriverDate;
        public string DriverVersion;
        public string VideoModeDescription;
        //public long CurrentNumberOfColors;
        //public int CurrentBitsPerPixel;
        //public int CurrentHorizontalResolution;
        //public int CurrentVerticalResolution;
        public int CurrentRefreshRate;
        public string AdapterDACType;
        public long AdapterRAM;
    }

    public struct HardwareTree
    {
        public BaseSystem BaseSystem;
        public DIMM[] Dimms;
        public Drive[] Drives;
        public LogicalVolume[] Volumes;
        public Processor CPU;
        public VideoProcessor VideoProcessor;
        public OHMData OHMData;
    }

    public class CmdQueueObject
    {
        public string id { get; set; }
        public string hostname { get; set; }
        public string command { get; set; }
    }

    public struct OHMData
    {
        public float CPUTemp;
        public float GPUTemp;

        public float CPULoad;
        public float GPULoad;
        public float GPUMemoryLoad;
        public float MemoryLoad;




    }

    public struct Settings
    {
        public string SDPwd;
        public string AdminPwd;
        public string Endpoint;
    }
}
