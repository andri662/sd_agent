﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;

namespace SD_AgentService
{
    class ProcessExecute
    {
        public List<string> output;
        public string plain_output;
        public string queued_cmd;
        public DBConnect db;
        private string AdminPwd;
        private string CmdTool, TimeFreezeTool;
        private static readonly HttpClient client = new HttpClient();

        public ProcessExecute()
        {
            this.db = new DBConnect();
            this.AdminPwd = ServiceLauncher.settings.AdminPwd;
            this.CmdTool = "CmdTool.exe";
            this.TimeFreezeTool = "C:\\Program Files\\Toolwiz Time Freeze 2017\\ToolwizTimeFreeze.exe";
        }

        public void Run(string cmd, string arguments)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            //psi.UserName = "Administrator";
            //psi.PasswordInClearText = AdminPwd;
            psi.FileName = cmd;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.Arguments = string.Format(@"{0}", arguments);
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            Process process = Process.Start(psi);

            //Log.Write("Running as: " + psi.UserName.ToString());
            Log.Write("File name: " + psi.FileName.ToString());

            List<string> output = new List<string>();
            string line = "";
//            this.plain_output = process.StandardOutput.ReadToEnd();
//            Log.Write(">> " + this.plain_output);

            while (!process.StandardOutput.EndOfStream)
            {
                line = process.StandardOutput.ReadLine();
                Log.Write("> " + line);
                output.Add(line);
            }
            
            this.output = output;

            process.WaitForExit();
            process.Close();
        }

        public void get_cmd_queue(string hostname)
        {
            this.queued_cmd = "";

            try
            {
                var responseString = client.GetStringAsync(ServiceLauncher.settings.Endpoint+"/poll_cmd/"+hostname).Result;
                if (responseString.Length == 0)
                    return;
                var result = JsonConvert.DeserializeObject<CmdQueueObject>(responseString);
                this.queued_cmd = result.command;
                Log.Write(result.hostname + " - " + result.id + " - " + result.command);
            }
            catch (Exception ex) {
                Log.Write(ex.Message);
            }
        }

        public void run_cmd_queue(string hostname)
        {
            Log.Write("run_cmd_queue(): Processing " + this.queued_cmd);
            if (this.queued_cmd == "tfenter" ) {
                Run(TimeFreezeTool, "/freezealways /usepass=" + ServiceLauncher.settings.SDPwd);
                Run("shutdown.exe", "-r -t 00");
            } else if (this.queued_cmd == "tfexit" ) {
                Run(TimeFreezeTool, "/unfreeze /usepass=" + ServiceLauncher.settings.SDPwd);
            } else if (this.queued_cmd.StartsWith("pcrename ")) {
                ServiceLauncher.SetMachineName(this.queued_cmd.Substring(9));
            } else if (this.queued_cmd.Length>0) {
                Log.Write("Begin Run() " + this.queued_cmd);
                Run("cmd.exe", "/c " + this.queued_cmd);
                Log.Write("End Run() " + this.queued_cmd);
            } else
            {
                return;
            }
            Log.Write("run_cmd_queue(): Executed command " + this.queued_cmd);
            var json_output = JsonConvert.SerializeObject(this.output);
            Log.Write(json_output);

            try {
                db.Query("UPDATE `sdcollect`.`sdc_commands` SET `output`='" + json_output + "', `executed`='1' WHERE `hostname`='" + hostname + "' AND `executed`='0'");
                Log.Write("Removed from queue with result:\n" + json_output);
            } catch (Exception ex) {
                Log.Write("Exception: " + ex.Message);
            }
        }
    }
}
