﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using OpenHardwareMonitor.Collections;
using OpenHardwareMonitor.Hardware;
using OxyPlot;
using OxyPlot.Series;

namespace SD_AgentService
{
    class Sensors
    {
        public string Hostname, AgentVersion;
        private Dictionary<string, ManagementObjectSearcher> wmiSearchers;

        private DateTime now;
        protected readonly ListSet<ISensor> active = new ListSet<ISensor>();
        public event SensorEventHandler SensorAdded;
        public event SensorEventHandler SensorRemoved;

        protected virtual void ActivateSensor(ISensor sensor)
        {
            if (active.Add(sensor))
                if (SensorAdded != null)
                    SensorAdded(sensor);
        }

        public Sensors()
        {

            this.wmiSearchers = new Dictionary<string, ManagementObjectSearcher>();

            string[] wmiClasses = {
                "Win32_Processor",
                "Win32_DiskDrive",
                "Win32_LogicalDisk",
                "Win32_PhysicalMemory",
                "Win32_VideoController",
                "Win32_BaseBoard"
            };

            foreach (string wmiClass in wmiClasses)
            {
                this.wmiSearchers.Add(wmiClass, new ManagementObjectSearcher(String.Format("SELECT * FROM {0}", wmiClass)));
            }

            this.Hostname = Dns.GetHostName();
            this.AgentVersion = "2.4.0";
        }


        // Requires administrative privileges
        public List<string> GetTemperature()
        {

            Log.Write("Getting temperature...");
            List<string> result = new List<string>();
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"root\WMI", "SELECT * FROM MSAcpi_ThermalZoneTemperature");
                foreach (ManagementObject obj in searcher.Get())
                {
                    Double temp = Convert.ToDouble(obj["CurrentTemperature"].ToString());
                    temp = (temp - 2732) / 10.0;
                    result.Add(temp.ToString());
                }
                return result;
            }
            catch
            {
                result.Add("N/A");
                return result;
            }
        }

        public OHMData GetOHMData()
        {
            OHMData data = new OHMData();

            var myComputer = new Computer();

            myComputer.CPUEnabled = true;
            myComputer.GPUEnabled = true;
            myComputer.RAMEnabled = true;
            myComputer.HDDEnabled = true;
            myComputer.ToCode();
            myComputer.Open();

            foreach (var hardwareItem in myComputer.Hardware)
            {
                hardwareItem.Update();
                //hardwareItem.GetReport();
                //textBox1.Text += hardwareItem.GetReport();

                var series = new LineSeries();

                foreach (var sensor in hardwareItem.Sensors)
                {
                    if (sensor.SensorType == SensorType.Load)
                    {
                        if (sensor.Name == "CPU Total")
                        {
                            data.CPULoad = (float)sensor.Value;
                        }

                        if (sensor.Name == "Memory")
                        {
                            data.MemoryLoad = (float)sensor.Value;
                        }

                        if (sensor.Name == "GPU Core")
                        {
                            data.GPULoad = (float)sensor.Value;
                        }

                        if (sensor.Name == "GPU Memory")
                        {
                            data.GPUMemoryLoad = (float)sensor.Value;
                        }
                    }

                    if (sensor.SensorType == SensorType.Temperature)
                    {
                        // Temp: CPU Package / OpenHardwareMonitor.Hardware.CPU.IntelCPU / Temperature / 37
                        if (sensor.Name == "CPU Package")
                        {
                            data.CPUTemp = (float)sensor.Value;
                        }

                        // Temp: Temperature / OpenHardwareMonitor.Hardware.HDD.SSDSamsung / Temperature / 34
                        // Temp: GPU Core / OpenHardwareMonitor.Hardware.Nvidia.NvidiaGPU / Temperature / 43
                        if (sensor.Name == "GPU Core")
                        {
                            data.GPUTemp = (float)sensor.Value;
                        }
                    }
                }
            }

            return data;
        }

        public string GetSData()
        {
            Log.Write("Getting Shadow Defender state...");
            ProcessExecute p = new ProcessExecute();
            string output = "";

            if (Directory.Exists("c:\\program files\\shadow defender"))
            {
                p.Run("c:\\program files\\shadow defender\\cmdtool.exe", "/pwd:" + ServiceLauncher.settings.SDPwd + " /list");
                if (p.output.Count > 0)
                {
                    //for (int i=0; i<p.output.Count; i++)
                    //{
                    //output.Concat(p.output[i]);
                    //}
                    foreach (string line in p.output)
                        output = output + line.Substring(0, 1);

                    return output;
                }
                else
                {
                    return "DISABLED";
                }
            }
            else
            {
                return "SD NOT FOUND";
            }
        }

        public string GetTFData()
        {
            Log.Write("Getting TimeFreeze state...");

            if (Directory.Exists("C:\\Program Files\\Toolwiz Time Freeze 2017"))
            {

                string current_protect_mode = ServiceLauncher.HKCU_GetString("HKEY_CURRENT_USER\\Software\\Toolwiz\\TimefreezeNew", "CURRENT_PROTECT_MODE");
                string next_boot = ServiceLauncher.HKCU_GetString("HKEY_CURRENT_USER\\Software\\Toolwiz\\TimefreezeNew", "NEXT_BOOT_PROTECT");

                if (current_protect_mode == "1")
                {
                    if (next_boot == "1")
                        return "ENABLED, ENABLED NEXT";
                    else
                        return "ENABLED, DISABLED NEXT";
                }
                else
                {
                    if (next_boot == "1")
                    {
                        return "DISABLED, ENABLED NEXT";
                    }
                    return "DISABLED, DISABLED NEXT";
                }
            }
            else
            {
                return "TIMEFREEZE NOT FOUND";
            }
        }

        public NetworkData GetNetworkData()
        {
            Log.Write("Getting network...");
            NetworkData net = new NetworkData();

            net.HardwareAddress = NetworkInterface
                    .GetAllNetworkInterfaces()
                    .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                    .Select(nic => nic.GetPhysicalAddress().ToString())
                    .FirstOrDefault();

            net.Hostname = Dns.GetHostName();

            net.IPv4 = Dns
                .GetHostEntry(net.Hostname)
                .AddressList
                .Where(addr => addr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                .Select(addr => addr.ToString())
                .LastOrDefault();

            IPInterfaceProperties data = NetworkInterface
                    .GetAllNetworkInterfaces()
                    .Where(nic => nic.OperationalStatus == OperationalStatus.Up && nic.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                    .Select(nic => nic.GetIPProperties())
                    .FirstOrDefault();
            net.DNS1 = data.DnsAddresses[0].ToString();

            if (data.DnsAddresses.Count() > 1)
            {
                net.DNS2 = data.DnsAddresses[1].ToString();
            }
            else
            {
                net.DNS2 = "";
            }

            return net;
        }

        public BaseSystem GetBaseSystemData()
        {
            Log.Write("Getting BaseSystem array...");
            BaseSystem system = new BaseSystem
            {
                AgentVersion = this.AgentVersion,
                OSName = (string)(from x in new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem").Get().Cast<ManagementObject>() select x.GetPropertyValue("Caption")).FirstOrDefault(),
                OSVersion = (string)(from x in new ManagementObjectSearcher("SELECT Version FROM Win32_OperatingSystem").Get().Cast<ManagementObject>() select x.GetPropertyValue("Version")).FirstOrDefault(),
                Baseboard = (string)(from x in new ManagementObjectSearcher("SELECT Product FROM Win32_BaseBoard").Get().Cast<ManagementObject>() select x.GetPropertyValue("Product")).FirstOrDefault(),
                BaseboardSerial = (string)(from x in new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard").Get().Cast<ManagementObject>() select x.GetPropertyValue("SerialNumber")).FirstOrDefault(),
                SDState = this.GetSData(),
                TFState = this.GetTFData()
            };

            return system;
        }

        #region CPU_INFORMATION

        public int getNumberOfCpus()
        {
            return new ManagementObjectSearcher("select * from Win32_Processor").Get().Count;
        }

        public Processor getCpuData(int cpuIndex = 0)
        {
            Log.Write("Getting CPUs...");
            Processor cpu = new Processor
            {
                ProcessorID = this.getValueFrom("Win32_Processor", "ProcessorID", cpuIndex).ToString(),
                CurrentClockSpeed = int.Parse(this.getValueFrom("Win32_Processor", "CurrentClockSpeed", cpuIndex)),
                MaxClockSpeed = int.Parse(this.getValueFrom("Win32_Processor", "MaxClockSpeed", cpuIndex)),
                Name = this.getValueFrom("Win32_Processor", "Name", cpuIndex).ToString(),
                Usage = Double.Parse(this.getValueFrom("Win32_Processor", "LoadPercentage")),
                PhysicalCores = int.Parse(this.getValueFrom("Win32_Processor", "NumberOfCores", cpuIndex)),
                LogicalCores = int.Parse(this.getValueFrom("Win32_Processor", "NumberOfLogicalProcessors", cpuIndex))
            };

            return cpu;
        }

        #endregion

        #region DIMM_INFORMATION

        public int getNumberOfDIMMs()
        {
            return _searcherAsList(this.wmiSearchers["Win32_PhysicalMemory"]).Count;
        }

        public DIMM[] GetDimmData(int dimmIndex = 0)
        {
            Log.Write("Getting DIMMs...");
            DIMM[] d = new DIMM[getNumberOfDIMMs()];
            for (int i = 0; i < getNumberOfDIMMs(); i++)
            {
                d[i].BankLabel = this.getValueFrom("Win32_PhysicalMemory", "BankLabel", i).ToString();
                d[i].PartNumber = this.getValueFrom("Win32_PhysicalMemory", "PartNumber", i).ToString();
                d[i].SerialNumber = this.getValueFrom("Win32_PhysicalMemory", "SerialNumber", i).ToString();
                d[i].Manufacturer = this.getValueFrom("Win32_PhysicalMemory", "Manufacturer", i).ToString();
                d[i].DeviceLocator = this.getValueFrom("Win32_PhysicalMemory", "DeviceLocator", i).ToString();
                d[i].Capacity = long.Parse(this.getValueFrom("Win32_PhysicalMemory", "Capacity", i));
                d[i].Speed = int.Parse(this.getValueFrom("Win32_PhysicalMemory", "Speed", i));
            }

            return d;
        }

        #endregion

        #region GRAPHICS_INFORMATION

        public VideoProcessor GetVideoData()
        {
            Log.Write("Getting VideoProcessor...");
            VideoProcessor card = new VideoProcessor
            {
                //CurrentRefreshRate = (from x in new ManagementObjectSearcher("SELECT CurrentRefreshRate FROM Win32_VideoController").Get().Cast<ManagementObject>() select x.GetPropertyValue("CurrentRefreshRate")).FirstOrDefault(),
                CurrentRefreshRate = int.Parse(this.getValueFrom("Win32_VideoController", "CurrentRefreshRate")),
                Description = this.getValueFrom("Win32_VideoController", "Description").ToString(),
                DriverDate = this.getValueFrom("Win32_VideoController", "DriverDate").ToString(),
                DriverVersion = this.getValueFrom("Win32_VideoController", "DriverVersion").ToString(),
                VideoModeDescription = this.getValueFrom("Win32_VideoController", "VideoModeDescription").ToString(),
                AdapterDACType = this.getValueFrom("Win32_VideoController", "AdapterDACType").ToString(),
                AdapterRAM = long.Parse(this.getValueFrom("Win32_VideoController", "AdapterRAM"))
            };
            return card;
        }
        #endregion

        #region DRIVE_INFORMATION

        public int getNumberOfDrives()
        {
            return _searcherAsList(this.wmiSearchers["Win32_DiskDrive"]).Count;
        }

        public int getNumberOfVolumes()
        {
            return _searcherAsList(this.wmiSearchers["Win32_LogicalDisk"]).Count;
        }

        public Drive[] GetDriveData(int driveIndex = 0)
        {
            Log.Write("Getting drives...");
            int counter = getNumberOfDrives();
            Drive[] d = new Drive[counter];

            for (int i = 0; i < counter; i++)
            {
                d[i].Caption = this.getValueFrom("Win32_DiskDrive", "Caption", i).ToString();
                d[i].DeviceID = this.getValueFrom("Win32_DiskDrive", "DeviceID", i).ToString();
                d[i].Partitions = int.Parse(this.getValueFrom("Win32_DiskDrive", "Partitions", i));
                d[i].Size = long.Parse(this.getValueFrom("Win32_DiskDrive", "Size", i));
                d[i].SerialNumber = this.getValueFrom("Win32_DiskDrive", "SerialNumber", i).ToString();
            }

            return d;

        }

        public LogicalVolume[] GetVolumeData(int diskIndex = 0)
        {
            Log.Write("Getting volumes...");
            int counter = getNumberOfVolumes();
            LogicalVolume[] vol = new LogicalVolume[counter];

            for(int i = 0; i < counter; i++)
            {
                vol[i].Letter = this.getValueFrom("Win32_LogicalDisk", "Name", i);
                vol[i].Label = this.getValueFrom("Win32_LogicalDisk", "VolumeName", i);
                vol[i].Size = long.Parse(this.getValueFrom("Win32_LogicalDisk", "Size", i));
                vol[i].FreeSpace = long.Parse(this.getValueFrom("Win32_LogicalDisk", "FreeSpace", i));
            }

            return vol;
        }
        #endregion

        #region PROCESSES

        public int getNumberOfProcesses()
        {
            return _searcherAsList(this.wmiSearchers["Win32_Process"]).Count;
        }

        public List<string> getProcessList()
        {
            Log.Write("Getting processes...");
            string[] unwanted =
            {
                "ServiceHub.DataWarehouseHost",
                "SecurityHealthService",
                "conhost",
                "ClassicStartMenu",
                "svchost",
                "Service",
                "winlogon",
                "TiWorker",
                "devenv",
                "SearchUI",
                "ABService",
                "SD_Agent",
                "ServiceHub.SettingsHost",
                "MSBuild",
                "ServiceHub.IdentityHost",
                "SgrmBroker",
                "ApplicationFrameHost",
                "MSASCuiL",
"MusNotifyIcon",
"Memory Compression",
"ServiceHub.TestWindowStoreHost",
"lsass",
"WmiPrvSE",
"services",
"ServiceHub.RoslynCodeAnalysisService32",
"SystemSettings",
"plugin_host",
"explorer",
"7zFM",
"Microsoft.Photos",
"RuntimeBroker",
"audiodg",
"DefenderDaemon",
"ShellExperienceHost",
"csrss",
"ServiceHub.Host.CLR.x86",
"wininit",
"sihost",
"dwm",
"NVDisplay.Container",
"Everything",
"fontdrvhost",
"TrustedInstaller",
"TeraCopyService",
"NisSrv",
"StandardCollector.Service",
"ctfmon",
"Microsoft.ServiceHub.Controller",
"Registry",
"ScriptedSandbox64",
"dasHost",
"MsMpEng",
"IPROSetMonitor",
"OpenWith",
"spoolsv",
"taskhostw",
"ServiceHub.VSDetouredHost",
"VBCSCompiler",
"smss",
"System",
"Idle"
            };


            Process[] processes = Process.GetProcesses();
            List<string> processList = new List<string>();

            foreach (Process theprocess in processes)
            {
                if (processList.Contains(theprocess.ProcessName))
                    continue;
                if (unwanted.Contains(theprocess.ProcessName))
                    continue;

                processList.Add(theprocess.ProcessName);
                //Log.Write("Process: {0} ID: {1}", theprocess.ProcessName, theprocess.Id);
            }

            return processList;
        }

        #endregion

        public HardwareTree GetHardwareTree()
        {
            Log.Write("Building HW tree...");
            HardwareTree tree = new HardwareTree
            {
                BaseSystem = this.GetBaseSystemData(),
                CPU = this.getCpuData(),
                Dimms = this.GetDimmData(),
                Drives = this.GetDriveData(),
                Volumes = this.GetVolumeData(),
                VideoProcessor = this.GetVideoData(),
                OHMData = this.GetOHMData(),
                //Temperature = this.GetTemperature()[0]
            };

            return tree;
        }

        // Utility method for getting the ManagementObjectCollection results as a List
        static List<ManagementBaseObject> _searcherAsList(ManagementObjectSearcher s)
        {
            List<ManagementBaseObject> tmp = new List<ManagementBaseObject>();
            foreach (var obj in s.Get())
            {
                tmp.Add(obj);
            }
            return tmp;
        }

        // Utility method for getting a property name from a wmiClass (for the given index)
        private string getValueFrom(string wmiClass, string propName, int itemIndex = 0)
        {
            return _searcherAsList(this.wmiSearchers[wmiClass])[itemIndex][propName].ToString();
        }
    }
}
