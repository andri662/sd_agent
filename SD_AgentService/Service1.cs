﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Management;
using System.ServiceProcess;
using System.Threading;
using System.Net.Http;

namespace SD_AgentService
{
    public partial class ServiceLauncher : ServiceBase
    {

        private static Sensors sensors;
        public static Settings settings;

        private static string hostname;
        private static long last_id;

        private static readonly HttpClient client = new HttpClient();

        public ServiceLauncher()
        {
            settings.Endpoint = "http://support.blink.al/gateway";
            settings.AdminPwd = "blink2018";
            settings.SDPwd = "blink2020";

            //Log.Write("Settings.AdminPwd : " + settings.AdminPwd);
            //Log.Write("Settings.SDPwd    : " + settings.SDPwd);
            Log.Write("Settings.Endpoint : " + settings.Endpoint);

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            sensors = new Sensors();    // Probes

            hostname = sensors.Hostname;

            Bootstrap();

            Thread processlist = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = false;
                // Stat collector
                System.Timers.Timer t_main = new System.Timers.Timer(60000);
                t_main.Elapsed += MainTick;
                t_main.Start();
            });
            processlist.Start();

            Thread command_queue = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = false;
                Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
                // Command poller
                System.Timers.Timer t_poll = new System.Timers.Timer(4000);
                t_poll.Elapsed += PollerTick;
                t_poll.Start();
            });
            command_queue.Start();
        }

        protected override void OnStop()
        {
        }

        private static void Bootstrap()
        {
            //settings.AdminPwd = HKLM_GetString(@"SOFTWARE\Blink", "AdminPwd");
            //settings.Endpoint = HKLM_GetString(@"SOFTWARE\Blink", "Endpoint");
            //settings.SDPwd = HKLM_GetString(@"SOFTWARE\Blink", "SDPwd");
            //Log.Write("Bootstrap Settings.AdminPwd: " + settings.AdminPwd);
            //Log.Write("Bootstrap Settings.Endpoint: " + settings.Endpoint);

            Log.Write("Init...");
            try
            {
                NetworkData net = sensors.GetNetworkData();
                insert_main(net.HardwareAddress, net.IPv4, net.Hostname);
                //db.Query(format_query("hardware", sensors.GetHardwareTree()));
                heartbeat_post("hardware", sensors.GetHardwareTree());
            } catch (Exception ex)
            {
                Log.Write("Message: " + ex.Message + "\nData: " + ex.Data);
            }
            Log.Write("Hardware data sent.");
        }

        private static void MainTick(object sender, EventArgs e)
        {
            // Update row
            NetworkData net = sensors.GetNetworkData();

            insert_main(net.HardwareAddress, net.IPv4, net.Hostname);

            // Get process list every minute
            //db.Query(format_query("processes", sensors.getProcessList()));
            heartbeat_post("processes", sensors.getProcessList());
        }

        private static void PollerTick(object sender, EventArgs e)
        {
            ProcessExecute p = new ProcessExecute();
            p.get_cmd_queue(hostname);
            p.run_cmd_queue(hostname);
        }

        public static void insert_main(string a, string b, string c)
        {
            string requestString = settings.Endpoint + "/heartbeat/" + a + "/" + b + "/" + c;
            Log.Write(requestString);
            var responseString = client.GetStringAsync(requestString).Result;
            //string query = string.Format("INSERT INTO `sdcollect_v2` (`timestamp`, `mac`, `ipv4`, `hostname`) VALUES( CURRENT_TIMESTAMP(), '{0}', '{1}', '{2}');", a, b, c);

            last_id = Int64.Parse(responseString);
        }

/*        public static string format_query(string dataclass, object dataobject)
        {

            string serialized = JsonConvert.SerializeObject(dataobject);

            string query = string.Format("INSERT INTO `sdcollect_v2_data` (`record_id`, `timestamp`, `hostname`, `class`, `data`) VALUES({0}, CURRENT_TIMESTAMP(), '{1}', '{2}', '{3}');",
                last_id,
                hostname,
                dataclass,
                serialized);

            return query;
        }*/

        public static void heartbeat_post(string dataclass, object dataobject)
        {
            string serialized = JsonConvert.SerializeObject(dataobject);

            var values = new Dictionary<string, string> {
                { "record_id", last_id.ToString() },
                { "hostname", hostname },
                { "class", dataclass },
                { "data", serialized }
            };

            var content = new FormUrlEncodedContent(values);

            var response = client.PostAsync(settings.Endpoint + "/heartbeat_post/", content);

            var responseString = response.Result;
            Log.Write(responseString.Content.ToString());
        }

        public static bool SetMachineName(string newName)
        {
            RegistryKey key = Registry.LocalMachine;

            string activeComputerName = "SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ActiveComputerName";
            RegistryKey activeCmpName = key.CreateSubKey(activeComputerName);
            activeCmpName.SetValue("ComputerName", newName);
            activeCmpName.Close();
            string computerName = "SYSTEM\\CurrentControlSet\\Control\\ComputerName\\ComputerName";
            RegistryKey cmpName = key.CreateSubKey(computerName);
            cmpName.SetValue("ComputerName", newName);
            cmpName.Close();
            string _hostName = "SYSTEM\\CurrentControlSet\\services\\Tcpip\\Parameters\\";
            RegistryKey hostName = key.CreateSubKey(_hostName);
            hostName.SetValue("Hostname", newName);
            hostName.SetValue("NV Hostname", newName);
            hostName.Close();
            return true;
        }

        public static string HKLM_GetString(string path, string key)
        {
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
                if (rk == null) return "";
                return (string)rk.GetValue(key);
            }
            catch (Exception e) { Log.Write(e.Message); return ""; }
        }

        public static string HKCU_GetString(string path, string key)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(path);
                if (rk == null) return "";
                return (string)rk.GetValue(key);
            }
            catch (Exception e) { Log.Write(e.Message); return ""; }
        }
    }
}
