﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SD_AgentService
{
    class Log
    {
        public static void Write(string str)
        {
            
            File.AppendAllText(Directory.GetCurrentDirectory() + "\\LogFiles\\SD_AgentService.log", DateTime.Now.ToString() + " : " + str + "\n");
        }
    }
}
