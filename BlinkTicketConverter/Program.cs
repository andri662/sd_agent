﻿using CommandLine;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Diagnostics;

namespace BlinkTicketConverter
{
    class Program
    {
        private static ManualResetEvent _quitEvent = new ManualResetEvent(false);
        //private static string hostname = Dns.GetHostName();
        private static string path = "C:\\ECR";
        private static string gateway = "http://46.101.165.8/gateway/";
        private static string default_product_id = "1000";

        private static string agent_location;
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private static string API_KEY;

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void Main(string[] args)
        {
            var hWND = GetConsoleWindow();
            ShowWindow(hWND, SW_HIDE);

            var key = ConfigurationManager.AppSettings["API_KEY"];

            Program.API_KEY = key;

            Debug.WriteLine("INITIALIZING WITH API_KEY " + Program.API_KEY);
            Parser.Default.ParseArguments<Options>((IEnumerable<string>)args).WithParsed<Options>((Action<Options>)(o =>
            {
                if (o.Location == null)
                    return;

                if (o.Path == null)
                    o.Path = Program.path;

                if (o.Path.Length > 0)
                    Program.path = o.Path;

                if (o.Location.Length > 0)
                {
                    Console.WriteLine("Generating for: --location " + o.Location);
                    Program.agent_location = o.Location;
                    System.Timers.Timer timer = new System.Timers.Timer(3500.0);
                    timer.Elapsed += new ElapsedEventHandler(Program.ProcessTicket);
                    timer.Start();
                }
                else
                {
                    Console.WriteLine("Current Arguments: --location " + o.Location);
                }
            }));
            Console.CancelKeyPress += (ConsoleCancelEventHandler)((sender, eArgs) =>
            {
                Program._quitEvent.Set();
                eArgs.Cancel = true;
            });
            Program._quitEvent.WaitOne();
        }

        private static async Task<string> GetResponseString(
     string api_call,
     Dictionary<string, string> parameters)
        {
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage response = await httpClient.PostAsync(Program.gateway + api_call, (HttpContent)new FormUrlEncodedContent((IEnumerable<KeyValuePair<string, string>>)parameters));
            string contents = await response.Content.ReadAsStringAsync();
            return contents;
        }

        private static void WriteFile(Ticket ticket, string filename)
        {
            int tax = 0;
            string out_id = "9000";
            string out_name = ticket.ProductName;
            if (ticket.ProductID == null)
            {
                out_id = Program.default_product_id;
            }
            else if (ticket.ProductID == "-")
            {
                out_id = Program.default_product_id;
            }
            else
            {
                out_id = ticket.ProductID;
            }

            out_name = out_name.Replace("Using ", "").Replace("for ", "");

            System.IO.File.AppendAllText(filename, "S,1,______,_,__;" + out_name.ToUpper() + ";" + ((int)ticket.NettoPrice).ToString() + ";" + ((int)ticket.Count).ToString() + ";1;1;" + tax.ToString() + ";0;" + out_id + ";\r\n");
            System.IO.File.AppendAllText(filename, "T,1,______,_,__;\r\n");

            return;
        }

        private static void ProcessTicket(object sender, EventArgs e)
        {
            foreach (Ticket ticket in (List<Ticket>)JsonConvert.DeserializeObject<List<Ticket>>(Program.GetResponseString("get_last_sale", new Dictionary<string, string>()
            {
                ["Location"] = Program.agent_location
            }).Result))
            {

                if (ticket.ProductName.Contains("Account"))
                    continue;

                string local_filename = Program.path + "\\" + FilenameGen() + ".txt";
                Program.WriteFile(ticket, local_filename);

                Debug.WriteLine("Ticket ID: " + ticket.ID + " - Filename: " + local_filename);

                Debug.WriteLine(Program.GetResponseString("ticket_print_flag", new Dictionary<string, string>()
                {
                    ["id"] = ticket.ID.ToString(),
                    ["flag"] = "1"
                }).Result);
            }

        }

        private static string FilenameGen()
        {
            Random rand = new Random();

            int randValue;
            string str = "";
            char letter;
            for (int i = 0; i < 32; i++)
            {
                randValue = rand.Next(0, 26);
                letter = Convert.ToChar(randValue + 65);
                str = str + letter;
            }
            return str;
        }

        public class Options
        {
            [Option(HelpText = "Location property.", Required = false)]
            public string Location { get; set; }

            [Option(HelpText = "Filename.", Required = false)]
            public string Filename { get; set; }

            [Option(HelpText = "Path.", Required = false)]
            public string Path { get; set; }
        }

        public struct Ticket
        {
            public long ID;
            public string ProductID;
            public string ProductName;
            public Decimal NettoPrice;
            public Decimal Count;
            public string Location;
        }
    }
}
