﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessNotifier
{
    class Program
    {
        static ManualResetEvent _quitEvent = new ManualResetEvent(false);

        static string hostname = Dns.GetHostName();
        static readonly HttpClient client = new HttpClient();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        public static string GetActiveProcessName()
        {
            IntPtr hwnd = GetForegroundWindow();
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            Process p = Process.GetProcessById((int)pid);
            //return p.MainModule.FileName;
            return p.ProcessName;
        }

        static void Main(string[] args)
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);

            Thread thread1 = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = false;
                Thread.CurrentThread.Priority = ThreadPriority.Normal;

                System.Timers.Timer t_poll = new System.Timers.Timer(10000);
                t_poll.Elapsed += PollerTick;
                t_poll.Start();
            });
            thread1.Start();
            PollerTick(null, null);
            //heartbeat_post("billing", GetActiveProcessName());
            //Console.WriteLine(GetActiveProcessName());


            Console.CancelKeyPress += (sender, eArgs) => {
                _quitEvent.Set();
                eArgs.Cancel = true;

            };

            _quitEvent.WaitOne();
        }

        private static void PollerTick(object sender, EventArgs e)
        {
            heartbeat_post("billing", GetActiveProcessName());
            //Console.WriteLine(GetActiveProcessName());
        }

        private static void heartbeat_post(string dataclass, object dataobject)
        {
            var values = new Dictionary<string, string> {
                { "record_id", "0" },
                { "hostname", hostname },
                { "class", dataclass },
                { "data", (string)dataobject }
            };

            var content = new FormUrlEncodedContent(values);

            var response = client.PostAsync("http://support.blink.al/gateway/heartbeat_post/?billing", content);
        }
    }
}
