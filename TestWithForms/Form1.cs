﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenHardwareMonitor.Collections;
using OpenHardwareMonitor.Hardware;
using OxyPlot;
using OxyPlot.Series;

namespace TestWithForms
{
    public partial class Form1 : Form
    {
        private DateTime now;
        protected readonly ListSet<ISensor> active = new ListSet<ISensor>();
        public event SensorEventHandler SensorAdded;
        public event SensorEventHandler SensorRemoved;

        protected virtual void ActivateSensor(ISensor sensor)
        {
            if (active.Add(sensor))
                if (SensorAdded != null)
                    SensorAdded(sensor);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();
            //Form1.ActiveForm.Hide();

            var myComputer = new Computer();

            myComputer.CPUEnabled = true;
            myComputer.GPUEnabled = true;
            myComputer.RAMEnabled = true;
            myComputer.HDDEnabled = true;
            myComputer.ToCode();
            myComputer.Open();

            foreach (var hardwareItem in myComputer.Hardware)
            {
                hardwareItem.Update();
                hardwareItem.GetReport();
                //textBox1.Text += hardwareItem.GetReport();


                var series = new LineSeries();

                foreach (var sensor in hardwareItem.Sensors)
                {
                    if (sensor.SensorType == SensorType.Load)
                    {
                        textBox1.Text += "Load: ";
                        textBox1.Text += sensor.Name + " / " + sensor.Hardware + " / " + sensor.SensorType + " / " + sensor.Value + "\r\n";

                    }

                    if (sensor.SensorType == SensorType.Fan)
                    {
                        textBox1.Text += "Fan: ";
                        textBox1.Text += sensor.Name + " / " + sensor.Hardware + " / " + sensor.SensorType + " / " + sensor.Value + "\r\n";

                    }

                    if (sensor.SensorType == SensorType.Temperature)
                    {
                        textBox1.Text += "Temp: ";
                        textBox1.Text += sensor.Name + " / " + sensor.Hardware + " / " + sensor.SensorType + " / " + sensor.Value + "\r\n";

                    }

/*                    if (sensor.SensorType == SensorType.Control)
                    {
                        textBox1.Text += "Control: ";
                        textBox1.Text += sensor.Name + " / " + sensor.Hardware + " / " + sensor.SensorType + " / " + sensor.Value + "\r\n";

                    }*/
                }
            }
            //textBox1.Text = GetActiveProcessFileName();
            //textBox1.Text = GetCaptionOfActiveWindow();

        }

        private void T1_Tick(object sender, EventArgs e)
        {
        }
    }
}
