﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ShadowClean
{
    class Program
    {
        static void Main(string[] args)
        {
            if (File.Exists("ShadowClean.txt"))
            {
                Console.WriteLine("Config found!");
                string[] subs;
                string[] separators = { "[SearchPaths]", "[Protected]" };
                string[] newline = { "\r\n", "\r", "\n" };
                string[] contents = File.ReadAllText("ShadowClean.ini").Split(separators, System.StringSplitOptions.RemoveEmptyEntries);

                string[] dirs = contents[0].Split(newline, System.StringSplitOptions.RemoveEmptyEntries);
                string[] exclusions = contents[1].Split(newline, StringSplitOptions.RemoveEmptyEntries);

                for (int i=0;i<dirs.Length;i++)
                {
                    if (Directory.Exists(dirs[i]))
                    {
                        subs = Directory.GetDirectories(dirs[i]);

                        for (int j=0;j<subs.Length;j++)
                        {
                            int match = 0;
                            foreach (string ex in exclusions)
                            {
                                if (subs[j].Contains(ex))
                                {
                                    match = 1;
                                }
                            }

                            if (match == 0)
                            {
                                DirectoryInfo di = new DirectoryInfo(subs[j]);
                                long dsize = di.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(fi => fi.Length);

                                Console.WriteLine("Cleaning up " + dsize);
                                Directory.Delete(subs[j], true);
                            }
                        }
                    }
                }
            }

        }
    }
}
